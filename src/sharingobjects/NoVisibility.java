package sharingobjects;

/**
 * @author 赵雨强
 * @create 2021/4/18 15:02
 */
public class NoVisibility {
    private static boolean ready;
    private static int number;

    private static class ReaderThread extends Thread{
        public void run(){
            while (!ready){
                Thread.yield();
            }
            if(number != 42){
                System.out.println(number);
            }

        }
    }

    public static void main(String[] args) {
        for (int i = 0; i < 100000; i++) {
            new ReaderThread().start();
            number=42;
            ready = true;
        }

    }
}
