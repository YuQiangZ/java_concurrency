package introduction;

/**
 * @author 赵雨强
 * @create 2021/4/18 11:43
 */
public class UnsafeSequence {

    private int value;

    public int getNext(){
        return value++;
    }
}
