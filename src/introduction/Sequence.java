package introduction;

/**
 * @author 赵雨强
 * @create 2021/4/18 11:48
 */
public class Sequence {
    private  int nextValue;

    public synchronized int getNextValue(){
        return nextValue++;
    }
}
